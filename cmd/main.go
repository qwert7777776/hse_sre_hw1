package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"gopkg.in/yaml.v2"
)

const (
	apiBaseURL = "http://localhost:8080/api/v0"
	baseURL    = "http://localhost:8080"
	//token      = "4877d8a4c9eeb6d6f62053d3063c6a12"
	//cookie     = "Webstorm-9a910f5e=dddee8ba-be06-4af8-b459-76182ea9d6ba; ajs_anonymous_id=8838f997-e91d-4f0c-8254-f2303bf85c9f; oncall-auth=c94a7385814c627c103c01c884e1ff63ba7c977e27mgkSCtt03lBtJ+PT92cw==7N9S4HDOML9aLddendFlRP6gPpKwBD/o9HeKh/UVfLyTHdQWkyW3YdzOiJVP6Gaxvvg5M2aCznI1R8FrGyWoEFS3Op+fGwX8Zkob+PV8c3wo91gvOWbRCHXv7MTLWltRY+tzLviIcP4OEPyX0MoSbT2TOD39GKYzypWRFVpvSQnDYsFdg4Mn7XJmr8zDw5KSQo8wbzcyiPQcp6HZlo3x1jvici3bJQLOeJ4cMvQ3U3DPHsHP9F1/hpms13zEU2OaJjHV9SbQBH9HLA=="
)

type Config struct {
	Teams []Team `yaml:"teams"`
}

type Team struct {
	Name               string `yaml:"name" json:"name"`
	SchedulingTimezone string `yaml:"scheduling_timezone" json:"scheduling_timezone"`
	Email              string `yaml:"email" json:"email"`
	SlackChannel       string `yaml:"slack_channel" json:"slack_channel"`
	Users              []User `yaml:"users" json:"users"`
}

type User struct {
	Name        string `yaml:"name" json:"name"`
	FullName    string `yaml:"full_name" json:"full_name"`
	PhoneNumber string `yaml:"phone_number" json:"phone_number"`
	Email       string `yaml:"email" json:"email"`
	Duty        []Duty `yaml:"duty" json:"duty"`
}

type Duty struct {
	Date string `yaml:"date" json:"date"`
	Role string `yaml:"role" json:"role"`
}

type Event struct {
	Start int    `json:"start"`
	End   int    `json:"end"`
	User  string `json:"user"`
	Team  string `json:"team"`
	Role  string `json:"role"`
}

func main() {
	config := Config{}

	file, err := ioutil.ReadFile("config.yaml")
	if err != nil {
		fmt.Println("Error reading YAML file:", err)
		return
	}

	err = yaml.Unmarshal(file, &config)
	if err != nil {
		fmt.Println("Error parsing YAML file:", err)
		return
	}

	for _, team := range config.Teams {
		err := createTeam(team)
		if err != nil {
			fmt.Println("Error creating team:", err)
			continue
		}

		err = createRoster(team)
		if err != nil {
			fmt.Println("Error creating roaster:", err)
			continue
		}

		for _, user := range team.Users {
			err := createUser(user)
			if err != nil {
				fmt.Println("Error creating user:", err)
				return
			}
			err = addUserToRoaster(team.Name, user)
			if err != nil {
				fmt.Println("Error adding user to roaster:", err)
				return
			}
			for _, d := range user.Duty {
				err = createEvent(team, user, d.Role, d.Date)
				if err != nil {
					fmt.Println("Error creating duty for:", user.Name, err)
					continue
				}
			}
		}
	}

	fmt.Println("Teams and users created successfully")
}

func createTeam(team Team) error {
	url := fmt.Sprintf("%s/teams", apiBaseURL)
	return sendPostRequest(url, team)
}

type Roster struct {
	Name string `json:"name"`
}

func createRoster(team Team) error {
	url := fmt.Sprintf("%s/teams/%s/rosters", apiBaseURL, team.Name)
	return sendPostRequest(url, Roster{Name: fmt.Sprintf("%s-r", team.Name)})
}

func addUserToTeam(teamName string, user User) error {
	url := fmt.Sprintf("%s/teams/%s/users", apiBaseURL, teamName)
	return sendPostRequest(url, user)
}

func addUserToRoaster(teamName string, user User) error {
	url := fmt.Sprintf("%s/teams/%s/rosters/%s-r/users", apiBaseURL, teamName, teamName)
	return sendPostRequest(url, Roster{Name: fmt.Sprintf("%s", user.Name)})
}

func toTime(ti string) int {
	layout := "02/01/2006" // Go uses a reference date format

	t, _ := time.Parse(layout, ti)
	return int(t.Unix())
}

type CreateUser struct {
	Name     string `yaml:"name" json:"name"`
	FullName string `yaml:"full_name" json:"full_name"`
}

func createUser(user User) error {
	url := fmt.Sprintf("%s/users", apiBaseURL)
	return sendPostRequest(url, CreateUser{
		Name:     user.Name,
		FullName: user.FullName,
	})
}

func createEvent(team Team, user User, role, date string) error {
	url := fmt.Sprintf("%s/events", apiBaseURL)
	return sendPostRequest(url, Event{
		Start: toTime(date),
		End:   toTime(date) + 3600*24,
		User:  user.Name,
		Team:  team.Name,
		Role:  role,
	})
}

func sendPostRequest(url string, data interface{}) error {
	jsonData, err := json.Marshal(data)
	if err != nil {
		return err
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonData))
	if err != nil {
		return err
	}

	//req.Header.Set("X-csrf-token", token)
	//req.Header.Set("Cookie", cookie)
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusCreated {
		body, _ := ioutil.ReadAll(resp.Body)
		return fmt.Errorf("API request failed with status %d: %s", resp.StatusCode, string(body))
	}

	return nil
}
